﻿
namespace TeamEightShop.Contracts.Dto
{
    /// <summary>
    /// Модель товара для отображения в списке товаров
    /// </summary>
    public class ProductDto
    {
        /// <summary>
        /// Id товара
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название товара
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Псевдоним продавца
        /// </summary>
        public string OwnerNikcName { get; set; }
    }
}
