﻿namespace TeamEightShop.Data.Data
{
    /// <summary>
    /// Интерфейс инициализации БД
    /// </summary>
    public interface IDbInitializer
    {
        /// <summary>
        /// Инициализация БД
        /// </summary>
        public void InitializeDb();
    }
}
