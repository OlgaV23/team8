﻿using TeamEightShop.Data.Entities;

namespace TeamEightShop.Data.Data
{
    /// <summary>
    /// Класс для заполнения БД тестовыми данными
    /// </summary>
    public static class FakeDataSeed
    {
        private static DateTime seedTime = DateTime.Parse("2022-02-19 12:15:00Z");

        public static List<Role> Roles => new List<Role>
        {
            new Role()
            {
                Id = Guid.Parse("d326a178-6c22-4171-bf76-ada1d44f6dfa"),
                Name = "Administrator",
                CreateDate = seedTime
            },
            new Role()
            {
                Id = Guid.Parse("b00a4ef9-a7f6-413c-a48c-e6e3034434ed"),
                Name = "StandartUser",
                CreateDate = seedTime
            }
        };


        public static List<User> Users => new List<User>
        {
            new User
            {
                NickName = "FirstTestUser",
                Id = Guid.Parse("0b0d2e1b-131b-4f84-991c-59c851fde6ca"),
                Email = "IvanIvanov@mail.ru",
                Password = "QWeRTY!",
                FirstName = "Ivan",
                LastName = "Ivanov",
                RoleId = Roles.First(r => r.Name == "StandartUser").Id,
                Address = "Россия, Москва, ул. Бульвар Капуцинов, д.35 кв. 2",
                CreateDate = seedTime
            },
            new User
            {
                NickName = "VnukEfiopa",
                Id = Guid.Parse("c6ec3181-01d9-43fc-a28d-1024c5d4f32d"),
                Email = "AleksandrPushkin@gmail.com",
                Password = "!QdAZ@S",
                FirstName = "Aleksandr",
                LastName = "Pushkin",
                RoleId = Roles.First(r => r.Name == "StandartUser").Id,
                Address = "Россия, Санкт-Петербург, с. Михайловское , ул. Советская, д.35",
            },
            new User
            {
                NickName = "HackerNeo",
                Id = Guid.Parse("e8b4590e-e473-4e66-b214-2e8ac9f8d51d"),
                Email = "SuperAdmin@ya.ru",
                Password = "Bmd5TgxF",
                FirstName = "Ivan",
                LastName = "Govnov",
                RoleId = Roles.First(r => r.Name == "Administrator").Id,
                Address = "Россия, Москва, ул.Боровицкая, Кремль",
                CreateDate = seedTime
            }
        };


        public static List<ProductType> ProductTypes => new List<ProductType>
        {
            new ProductType
            {
                Name = "Sweater",
                Id = Guid.Parse("203c13ac-9b32-4b35-af10-f112eadde2bc"),
                CreateDate = seedTime
            },
            new ProductType
            {
                Name = "Hoodie",
                Id = Guid.Parse("4f5c0967-2233-41bd-9d7f-a609c1f13401"),
                ParentProductTypeId = Guid.Parse("203c13ac-9b32-4b35-af10-f112eadde2bc"),
                CreateDate = seedTime
            },
            new ProductType
            {
                Name = "Sweatshirt",
                Id = Guid.Parse("87e6a206-f98c-47ad-b055-587bd82b92d3"),
                ParentProductTypeId = Guid.Parse("203c13ac-9b32-4b35-af10-f112eadde2bc"),
                CreateDate = seedTime
            },
            new ProductType
            {
                Name = "The nether man",
                Id = Guid.Parse("40876f04-30ce-45f3-8402-2b6f8b7ea250"),
                CreateDate = seedTime
            },
            new ProductType
            {
                Name = "Sweatpant",
                Id = Guid.Parse("24fe9f00-aedc-4da0-99e7-aed00fbcbb5a"),
                ParentProductTypeId = Guid.Parse("40876f04-30ce-45f3-8402-2b6f8b7ea250"),
                CreateDate = seedTime
            },

        };


        public static List<Product> Products => new List<Product>
        {
            new Product()
            {
                Name = "Vlone Bad Habits Hoodie",
                Description = "Худи Bad Habits",
                Price = (decimal)160.44,
                Type = ProductTypes.First(r => r.Name == "Hoodie"),
                Owner = Users.First(u =>u.Email == "IvanIvanov@mail.ru"),
                State = State.Active,
                Id = Guid.Parse("285e19c1-80da-4739-b15e-5998124312d3"),
                CreateDate = seedTime
            },
            new Product()
            {
                Name = "Travis Scott Jordan Cactus Jack Highest Hoodie",
                Description = "Худи Travis Scott Jordan Cactus Jack Highest",
                Price = (decimal)555.55,
                Type = ProductTypes.First(r => r.Name == "Hoodie"),
                Owner = Users.First(u =>u.Email == "IvanIvanov@mail.ru"),
                State = State.Inactive,
                Id = Guid.Parse("575d127e-8941-43c0-b385-9b5bb1d86f3b"),
                CreateDate = seedTime
            },
            new Product()
            {
                Name = "Sp5der Websuit Sweatpant",
                Description = "Штаны Sp5der Websuit",
                Price = (decimal)60.00,
                Type = ProductTypes.First(r => r.Name == "Sweatpant"),
                Owner = Users.First(u =>u.Email == "AleksandrPushkin@gmail.com"),
                State = State.Active,
                Id = Guid.Parse("575d127e-8941-43c0-b385-9b5bb1d86f3b"),
                CreateDate = seedTime
            },


        };
    }
}
