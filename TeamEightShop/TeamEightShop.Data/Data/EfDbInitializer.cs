﻿using TeamEightShop.Data.Contexts;

namespace TeamEightShop.Data.Data
{
    /// <summary>
    /// Инициализация БД
    /// </summary>
    public class EfDbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.Products.AddRange(FakeDataSeed.Products);


            _dataContext.SaveChanges();
        }
    }
}
