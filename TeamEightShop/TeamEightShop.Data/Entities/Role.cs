﻿namespace TeamEightShop.Data.Entities
{
    /// <summary>
    /// Сущность роли для БД
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// Название роли
        /// </summary>
        public string Name { get; set; }
    }
}
