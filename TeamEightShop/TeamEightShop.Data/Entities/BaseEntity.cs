﻿namespace TeamEightShop.Data.Entities
{
    /// <summary>
    /// Базовый класс для сущностей БД с простым ПК
    /// </summary>
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
    }
}