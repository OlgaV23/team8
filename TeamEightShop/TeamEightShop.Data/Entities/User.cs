﻿

/// <summary>
/// Сущность пользователя для БД
/// </summary>
namespace TeamEightShop.Data.Entities
{
    public class User : BaseEntity
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Псевдоним (отображается в качестве основого имени пользвателя)
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// ID роли
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Роль
        /// </summary>
        public virtual Role Role { get; set; }
        
        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
