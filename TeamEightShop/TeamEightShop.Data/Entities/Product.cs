﻿namespace TeamEightShop.Data.Entities
{
    /// <summary>
    /// Сущность товара для БД
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// Название товара
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание товара
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// ID типа товара
        /// </summary>
        public Guid TypeId { get; set; }

        /// <summary>
        /// Тип товара
        /// </summary>
        public virtual ProductType Type { get; set; }

        /// <summary>
        /// ID продавца
        /// </summary>
        public Guid OwnerId { get; set; }

        /// <summary>
        /// Продавец
        /// </summary>
        public virtual User Owner { get; set; }

        /// <summary>
        /// Состояние активности товарной позиции.
        /// </summary>
         public State State { get; set; }

    }

    public enum State
    {
        Inactive,
        Active
    }
}
