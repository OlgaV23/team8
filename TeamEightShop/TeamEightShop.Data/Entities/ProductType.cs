﻿namespace TeamEightShop.Data.Entities
{
    /// <summary>
    /// Сущность типа продукта для БД 
    /// (если ParentProductTypeId не указан, то это основной тип (верхняя одежда, штаны, обувь и т.д.),
    /// если ParentProductTypeId указан, то это подтип (куртка, свитер, худи - подтипы верхней одежды))
    /// </summary>
    public class ProductType : BaseEntity
    {
        /// <summary>
        /// Название типа продукта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID родительской категории продуктов.
        /// </summary>
        public Guid? ParentProductTypeId { get; set; }

        /// <summary>
        /// Коллекция типов товара потомков данного типа. 
        /// </summary>
        public virtual ICollection<ProductType>? ChildProductTypes { get; set; }
    }
}
