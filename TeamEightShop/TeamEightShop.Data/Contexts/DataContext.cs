﻿using Microsoft.EntityFrameworkCore;
using TeamEightShop.Data.Entities;

namespace TeamEightShop.Data.Contexts
{
    /// <summary>
    /// Контекст БД
    /// </summary>
    public class DataContext : DbContext, IDataContext
    {
        /// <summary>
        /// Таблина пользователей
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Таблица ролей
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Таблица товаров
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Таблица типов товаров
        /// </summary>
        public DbSet<ProductType> ProductTypes { get; set; }

        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
                
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        /// <summary>
        /// Конфигурация модели с предварительным соглашением https://docs.microsoft.com/ru-ru/ef/core/what-is-new/ef-core-6.0/whatsnew#pre-convention-model-configuration
        /// </summary>
        /// <param name="configurationBuilder"></param>
        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.Properties<string>().HaveMaxLength(250);
        }


        /// <summary>
        /// Построение связей между сущностями БД
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Product>(p =>
            {
                p.HasOne(x => x.Owner)
                    .WithMany(x => x.Products)
                    .HasForeignKey(x => x.OwnerId);
                p.Property(p => p.Description).HasMaxLength(500);

            });

            modelBuilder.Entity<ProductType>()
                .HasMany(x => x.ChildProductTypes)
                .WithOne()
                .HasForeignKey(x => x.ParentProductTypeId);


            //
            // User
            //
            modelBuilder.Entity<User>(x =>
            {
                x.HasOne<Role>();
                x.Property(u => u.Email).IsRequired();
                x.Property(u => u.Password).IsRequired();
            });
        }

    }
}
