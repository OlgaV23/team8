﻿using Microsoft.EntityFrameworkCore;
using TeamEightShop.Data.Contexts;
using TeamEightShop.Data.Entities;
using TeamEightShop.Data.Repositories.Interfaces;

namespace TeamEightShop.Data.Repositories.Implimentation
{
    /// <summary>
    /// Класс реализации IBaseEntityRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseEntityRepository<T> : Repository, IBaseEntityRepository<T>
    where T:BaseEntity
    {
        
        public BaseEntityRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        private readonly DataContext _dataContext;

        #region Methods to Database

        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dataContext.Set<T>().ToListAsync();
            return entities;
        }

        
        public Task<T> GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        
        public Task CreateAsync(T item)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T item)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}
