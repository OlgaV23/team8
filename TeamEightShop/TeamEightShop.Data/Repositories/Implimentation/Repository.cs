﻿using TeamEightShop.Data.Contexts;
using TeamEightShop.Data.Repositories.Interfaces;

namespace TeamEightShop.Data.Repositories.Implimentation
{
    /// <summary>
    /// Класс реализации IRepository
    /// </summary>
    public class Repository : IRepository
    {
        public Repository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        private readonly DataContext _dataContext;
        
        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dataContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
