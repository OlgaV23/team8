﻿using TeamEightShop.Data.Entities;

namespace TeamEightShop.Data.Repositories.Interfaces
{
    /// <summary>
    /// Базовый интерфейс для CRUD операций
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseEntityRepository<T> : IRepository
    where T : BaseEntity
    {
        /// <summary>
        /// Получить полный список сущностей
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Создать запись в БД
        /// </summary>
        /// <param name="item">Сущность, которую нужно добавить в БД</param>
        /// <returns></returns>
        Task CreateAsync(T item);

        /// <summary>
        /// Изменить существую записть в БД
        /// </summary>
        /// <param name="item">Изменная сущность</param>
        /// <returns></returns>
        Task UpdateAsync(T item);

        /// <summary>
        /// Удалить запись в БД по ID
        /// </summary>
        /// <param name="id">ID записи для удаления</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);


    }
}
