﻿namespace TeamEightShop.Data.Repositories.Interfaces
{
    /// <summary>
    /// Базовый интерфейс, который наследует IDisposable
    /// </summary>
    public interface IRepository : IDisposable
    {

    }
}
