﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TeamEightShop.Data.Entities;
using TeamEightShop.Data.Repositories.Interfaces;

namespace TeamEightShop.WebHost.Controllers
{
    /// <summary>
    /// Контроллер товаров
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IBaseEntityRepository<Product> _productRepository;

        public ProductController(IBaseEntityRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetProductsAsync()
        {
            // временное решенине для проверки получения данных
            var productList = await _productRepository.GetAllAsync();

            var res = productList.Select(x => new Product()
            {
                Id = x.Id,
                OwnerId = x.OwnerId,
                Price = x.Price,
                TypeId = x.TypeId,
                Name = x.Name
            }).ToList();


            return res;
        }
    }
}
